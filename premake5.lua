workspace "Karma"
   architecture "x86_64"
   startproject "karma"
   configurations { "Debug", "Developer", "Release" }

group "extern"
    include "modules/freetype"
group ""

project "karma"
   kind "WindowedApp"
   language "C++"
   cdialect "C17"
   cppdialect "C++20"
   characterset "Unicode"

   targetdir ("%{wks.location}/.build/" .. "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}")
   objdir ("%{wks.location}/.build/ints/" .. "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}" .. "/%{prj.name}")
   debugdir ("%{wks.location}")

   files
   {
      "src/**.h", "src/**.cpp", "src/**.c",
      "modules/media/**.h", "modules/media/**.cpp", "modules/media/**.natvis", "modules/media/**.natstepfilter", "modules/media/.clang-format",
      "src/%{prj.name}.ico",
      "src/%{prj.name}.rc",
      "src/%{prj.name}.exe.manifest"
   }

   includedirs { "modules/media", "modules/freetype/include" }

   links { "freetype" }

   dpiawareness "HighPerMonitor"

   filter "configurations:Debug"
      defines { "DEBUG", "K_BUILD_DEBUG" }
      symbols "On"
      runtime "Debug"

   filter "configurations:Developer"
      defines { "NDEBUG", "K_BUILD_DEVELOPER" }
      optimize "On"
      runtime "Release"

   filter "configurations:Release"
      defines { "NDEBUG", "K_BUILD_RELEASE", "IMGUI_DISABLE" }
      optimize "On"
      runtime "Release"

   filter "system:windows"
      systemversion "latest"
      defines { "_CRT_SECURE_NO_WARNINGS" }
      flags { "NoManifest" }
