#include "kMedia.h"
#include "kMath.h"
#include "kImGui.h"

#include "stb/stb_truetype.h"

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_SYSTEM_H
#include FT_MODULE_H
#include FT_OUTLINE_H

static r32 FrequencyTable[] = {
    261.63f,
    293.66f,
    329.63f,
    349.23f,
    392.00f,
    440.00f,
    493.88f,
    523.25f,
    0,
};

static kKeyCode FrequencyKeys[9] = {
    kKeyCode::A,
    kKeyCode::S,
    kKeyCode::D,
    kKeyCode::F,
    kKeyCode::G,
    kKeyCode::H,
    kKeyCode::J,
    kKeyCode::K,
    kKeyCode::L,
};

enum class MusicNote
{
    C4,
    D4,
    E4,
    F4,
    G4,
    A4,
    B4,
    C5,
    NOSOUND,
};

struct FrequencyFactor
{
    r32 Value;
    r32 Target;
};

struct AudioContext
{
    int             FrameIndex              = 0;
    int             NoteIndex               = 0;
    int             NoteIter                = 0;

    FrequencyFactor FrequencyFactorTable[9] = {};
};

#ifndef IMGUI_DISABLE

void ImGuiUpdate(void)
{
    // ImGui::ShowDemoWindow();
}

#else
void ImGuiUpdate(void) {}
#endif

r32 AnimateFrequencyAmplitude(FrequencyFactor &factor, r32 dt)
{
    factor.Value = kExpDecay(factor.Value, factor.Target, 0.001f, dt);
    return factor.Value;
}

static kSpan<stbtt_vertex> GlyphVertices;
static stbtt_fontinfo      FontInfo;
FT_Outline                *GlyphOutline;
static kPathBuilder2D      Builder;

//
//
//

static r32 scale_font = 0.1f;

static int FT_Outline_MoveTo(const FT_Vector *to, void *user)
{
    kPathBuilder2D *builder = (kPathBuilder2D *)user;
    kVec2           p;
    p.x = scale_font * (r32)to->x / 64.0f;
    p.y = scale_font * (r32)to->y / 64.0f;
    kPathMoveTo(builder, p);
    return 0;
}

static int
FT_Outline_LineTo(const FT_Vector *to, void *user)
{
    kPathBuilder2D *builder = (kPathBuilder2D *)user;
    kVec2           p;
    p.x = scale_font * (r32)to->x / 64.0f;
    p.y = scale_font * (r32)to->y / 64.0f;
    kPathLineToMergeDuplicate(builder, p);
    return 0;
}

static int FT_Outline_ConicTo(const FT_Vector *control, const FT_Vector *to, void *user)
{
    kPathBuilder2D *builder = (kPathBuilder2D *)user;
    kVec2           b;
    b.x = scale_font * (r32)control->x / 64.0f;
    b.y = scale_font * (r32)control->y / 64.0f;
    kVec2 c;
    c.x = scale_font * (r32)to->x / 64.0f;
    c.y = scale_font * (r32)to->y / 64.0f;
    kPathBezierQuadraticTo(builder, b, c);
    return 0;
}

static int FT_Outline_CubicTo(const FT_Vector *control1, const FT_Vector *control2, const FT_Vector *to, void *user)
{
    kPathBuilder2D *builder = (kPathBuilder2D *)user;
    kVec2           b;
    b.x = scale_font * (r32)control1->x / 64.0f;
    b.y = scale_font * (r32)control1->y / 64.0f;
    kVec2 c;
    c.x = scale_font * (r32)control2->x / 64.0f;
    c.y = scale_font * (r32)control2->y / 64.0f;
    kVec2 d;
    d.x = scale_font * (r32)to->x / 64.0f;
    d.y = scale_font * (r32)to->y / 64.0f;
    kPathBezierCubicTo(builder, b, c, d);
    return 0;
}

static FT_Outline_Funcs OutlineInterface = {FT_Outline_MoveTo, FT_Outline_LineTo, FT_Outline_ConicTo, FT_Outline_CubicTo};

void                    UpdateFrame(float ft)
{
    AudioContext *ctx = (AudioContext *)kGetClientData();

    kHandleDefaultEvents();

    if (kKeyPressed(kKeyCode::Space))
    {
        if (kGetAudioState(kAudioDeviceType::Render) == kAudioState::Resumed)
            kPauseAudioRender();
        else
            kResumeAudioRender();
    }

    for (u32 iter = 0; iter < kArrayCount(FrequencyTable); ++iter)
    {
        if (kKeyDown(FrequencyKeys[iter]))
        {
            ctx->FrequencyFactorTable[iter].Target = 1;
        }
        else
        {
            ctx->FrequencyFactorTable[iter].Target = 0;
        }
    }

    kBeginRenderPass();

    kBeginCamera2D(kIdentity());

    static bool        closed         = false;
    static kStrokeJoin join           = kStrokeJoin::Round;
    static kStrokeCap  cap            = kStrokeCap::Round;
    static kStrokeType type           = kStrokeType::Dashed;
    static r32         dash           = 80;
    static r32         space          = 25;
    static r32         offset         = 0;
    static bool        offset_animate = true;
    static int         num_segments   = 0;
    static r32         stroke_width   = 8;
    static kVec4       stoke_color    = kVec4(0.5f, 0.8f, 0.1f, 1.0f);

    // kFillRectCentered(kVec2(-0.3f), kVec2(0.5f));
    // kBreakCommand2D();
    // kFillRectCentered(kVec2(0.3f), kVec2(0.5f));

#if 1
    if (offset_animate)
        offset += 1.0f;
    offset = kMod(offset, space + dash);

    kStrokeWidth(stroke_width);
    kStrokeStyleJoin(join);
    kStrokeStyleCap(cap);
    kStrokeStyleType(type);
    kStrokeDash(dash, space, offset);

    kRenderColor(stoke_color);

    // kPathMoveTo(kVec2(0));
    // kPathLineTo(kVec2(0.7f));
    // kPathLineTo(kVec2(0.7f, 0.2f));
    // kPathLineTo(kVec2(1.0f, 0.0f));
    // if (closed)
    //     kPathClose();
    // kStrokePath();

    // kPathMoveTo(kVec2(-0.2f));
    // kPathLineTo(kVec2(-0.7f, -0.2f));
    // kPathLineTo(kVec2(-0.9f, 0.5f));
    // kPathLineTo(kVec2(-0.3f, 0.6f));
    // if (closed)
    //     kPathClose();
    // kStrokePath();

    kPathMoveTo(kVec2(-0.5f, -0.5f));
    kPathLineTo(kVec2(-0.8f, -0.8f));
    kPathLineTo(kVec2(-0.1f, -0.9f));
    if (closed)
        kPathClose();
    kStrokePath();

    kPathMoveTo(kVec2(0.0f, -0.7f));
    kPathLineTo(kVec2(0.4f, -0.7f));
    kPathLineTo(kVec2(0.7f, -0.7f));
    kPathLineTo(kVec2(0.7f, -0.9f));
    kPathLineTo(kVec2(0.7f, -0.4f));
    if (closed)
        kPathClose();
    kStrokePath();

    static kVec2 Points[3] = {kVec2(-0.7f), kVec2(0.5f, 0.7f), kVec2(-0.6f, 0.4f)};

    kPushClipRect(0, 0, kGetWindowSize().x / 2, kGetWindowSize().y / 2);
    kPushClipRect(0, 0, kGetWindowSize().x / 2, kGetWindowSize().y / 2);

    kStrokeBezierQuadratic(Points[0], Points[1], Points[2]);

    kPopClipRect();
    kPopClipRect();

    FT_Outline_Decompose(GlyphOutline, &OutlineInterface, &Builder);
    kPathEnd(&Builder);

    kStrokeBuilder(Builder);

    kStrokePath();

    kReset(&Builder);
#endif

    // kPathMoveTo(Points[0]);
    // kPathBezierQuadraticTo(Points[1], Points[2]);
    //// kPathNext();

    // kRenderColor(kVec4(1, 0, 0, 1));

    // kSpan<kVec2> path = kTopPath();
    // for (const kVec2 &p : path)
    //{
    //     kFillCircle(p, 8.0f * kCameraPixelFactor2D());
    // }

    // kPopPath();

#if 0
    // kStrokeIsClosed(1);

    r32 scale = stbtt_ScaleForPixelHeight(&FontInfo, 1) * scale_font; // * kCameraPixelFactor2D();

    // r32 scale_xx = 1;

    // kPathPropertyBezierTolerance(kGetMediaIo().Render.PathProperties->BezierTolerancePixels / scale_xx);
    // kPathPropertyArcTolerance(kGetMediaIo().Render.PathProperties->ArcTolerancePixels / scale_xx);

    // kPushScale(scale_xx);

    // kBeginPath();

    // kPathLineTo(kVec2(0));

    FT_Outline_Decompose(GlyphOutline, &OutlineInterface, 0);

#if 0
    for (const stbtt_vertex &vertex : GlyphVertices)
    {
        kVec2 p;
        p.x = scale * vertex.x;
        p.y = scale * vertex.y;

        if (vertex.type == STBTT_vmove)
        {
            kPathMoveTo(p);
        }
        else if (vertex.type == STBTT_vline)
        {
            kPathLineTo(p);
        }
        else if (vertex.type == STBTT_vcurve)
        {
            kVec2 c;
            c.x = scale * vertex.cx;
            c.y = scale * vertex.cy;
            kPathBezierQuadraticTo(c, p, num_segments);
        }
        else if (vertex.type == STBTT_vcubic)
        {
            kVec2 c1;
            c1.x = scale * vertex.cx;
            c1.y = scale * vertex.cy;
            kVec2 c2;
            c2.x = scale * vertex.cx1;
            c2.y = scale * vertex.cy1;
            kPathBezierCubicTo(c1, c2, p, num_segments);
        }
    }
#endif

    kStrokePath();
#endif

    if (ImGui::Begin("Settings"))
    {
        const char *JoinNames[] = {"miter", "bevel", "round"};
        const char *CapNames[]  = {"butt", "square", "round"};
        const char *TypeNames[] = {"normal", "dashed"};

        ImGui::Checkbox("Close Loop", &closed);
        ImGui::Checkbox("Animate Offset ", &offset_animate);

        ImGui::Combo("Join Type", (int *)&join, JoinNames, kArrayCount(JoinNames));
        ImGui::Combo("Cap Type", (int *)&cap, CapNames, kArrayCount(CapNames));
        ImGui::Combo("Stroke Type", (int *)&type, TypeNames, kArrayCount(TypeNames));
        ImGui::DragFloat("Dash", &dash, 0.01f, 0.0f, 500.0f);
        ImGui::DragFloat("Space", &space, 0.01f, 0.0f, 500.0f);
        ImGui::DragFloat("Offset", &offset, 0.01f, 0.0f, 500.0f);
        ImGui::DragInt("Segments", &num_segments, 0.01f, 0, 2056);
        ImGui::DragFloat("Scale", &scale_font, 0.01f, 0.01f, 50);
        ImGui::DragFloat("Stroke Width", &stroke_width, 0.01f, 0.01f, 50);
        ImGui::ColorEdit4("Stroke Color", stoke_color.m);
    }
    ImGui::End();

    kEndCamera2D();

    kEndRenderPass();

    ImGuiUpdate();

    kThreadResetScratchpad();
}

u32 RenderAudio(kAudioFrames frames, const kAudioSpec *spec)
{
    AudioContext *ctx   = (AudioContext *)kGetClientData();

    r32          *frame = frames.Data;
    for (u32 iter = 0; iter < frames.Count; ++iter)
    {
        r32 t     = (r32)ctx->FrameIndex / (r32)spec->Frequency;

        r32 value = 0;

        r32 dt    = 1.0f / spec->Frequency;

        for (int i = 0; i < kArrayCount(FrequencyTable); ++i)
        {
            r32 freq = FrequencyTable[i] * AnimateFrequencyAmplitude(ctx->FrequencyFactorTable[i], dt);
            value += kSin(freq * t);
        }

        for (u32 channel = 0; channel < spec->Channels; ++channel)
        {
            frame[channel] = 0.4f * value;
        }
        frame += frames.Stride;
        ctx->FrameIndex += 1;
    }

    return frames.Count;
}

struct kFreeType
{
    FT_Library Library;
};

static void *kFreeTypeAlloc(FT_Memory mem, long size)
{
    return kAlloc(kGetMediaContextAllocator(), size);
}

static void kFreeTypeFree(FT_Memory memory, void *ptr)
{
    kFree(kGetMediaContextAllocator(), ptr);
}

static void *kFreeTypeRealloc(FT_Memory memory, long prev_size, long new_size, void *ptr)
{
    return kRealloc(kGetMediaContextAllocator(), ptr, new_size, prev_size);
}

void Load(void)
{
    kFreeType     FreeType = {};

    FT_MemoryRec_ mem      = {};
    mem.alloc              = kFreeTypeAlloc;
    mem.free               = kFreeTypeFree;
    mem.realloc            = kFreeTypeRealloc;

    FT_Error err           = 0;

    if (err = FT_New_Library(&mem, &FreeType.Library))
    {
        kLogError("FreeType: %s", FT_Error_String(err));
    }

    FT_Add_Default_Modules(FreeType.Library);

    kString font = kDefaultFontRawData(); // must not deallocate the memory before calling FT_Done_Face

    FT_Face face;
    if (err = FT_New_Memory_Face(FreeType.Library, font.Data, (FT_Long)font.Count, 0, &face))
    {
        kLogError("FreeType: %s", FT_Error_String(err));
    }

    if (err = FT_Select_Charmap(face, FT_ENCODING_UNICODE))
    {
        kLogError("FreeType: %s", FT_Error_String(err));
    }

    FT_UInt            glyph_index = FT_Get_Char_Index(face, 'a');

    FT_Size_RequestRec req_size    = {};
    req_size.type                  = FT_SIZE_REQUEST_TYPE_REAL_DIM;
    req_size.width                 = 0;
    req_size.height                = 20 * 64;
    req_size.horiResolution        = 0;
    req_size.vertResolution        = 0;

    if (err = FT_Request_Size(face, &req_size))
    {
        kLogError("FreeType: %s", FT_Error_String(err));
    }

    if (glyph_index)
    {
        FT_UInt32 load_flags = FT_LOAD_COLOR | FT_LOAD_TARGET_NORMAL | FT_LOAD_RENDER;
        if (err = FT_Load_Glyph(face, glyph_index, load_flags))
        {
            kLogError("FreeType: %s", FT_Error_String(err));
        }

        // kPathBuilder2D builder;

        // kEmbossConfig config;
        // config.StrokeJoin   = kStrokeJoin::Round;
        // config.StrokeCap    = kStrokeCap::Round;
        // config.Radius       = 0.1f;
        // config.MiterLimitSq = 0.1f;

        // auto iter           = kIter(&builder);
        // while (kNext(&iter))
        //{
        //     kPathEmbossLines(&Builder, iter.Value, config);
        // }

        // kFree(&builder);
        GlyphOutline = &face->glyph->outline;
    }
}

int Main(int argc, char **argv)
{
    kString content = kDefaultFontRawData();
    stbtt_InitFont(&FontInfo, content.Data, 0);

    int index           = stbtt_FindGlyphIndex(&FontInfo, 'a');
    GlyphVertices.Count = stbtt_GetGlyphShape(&FontInfo, index, &GlyphVertices.Data);

    kMediaCallback fn   = {};
    fn.Update           = UpdateFrame;
    fn.RenderAudio      = RenderAudio;
    fn.Load             = Load;

    AudioContext ac     = {};
    int          rc     = kEventLoop(fn, &ac);

    return rc;
}
